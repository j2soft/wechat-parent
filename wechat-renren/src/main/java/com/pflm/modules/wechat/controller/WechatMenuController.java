package com.pflm.modules.wechat.controller;
import com.alibaba.fastjson.JSONObject;
import com.pflm.api.WeChatApi;
import com.pflm.common.utils.R;
import com.pflm.modules.wechat.entity.WxMenuEntity;
import com.pflm.modules.wechat.service.WxMenService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信自定义菜单
 * @author qinxuewu
 * @version 1.00
 * @time 16/11/2018下午 1:22
 */
@RestController
@RequestMapping("/sys/wechatmenu")
public class WechatMenuController{

    @Autowired
    WxMenService wxMenService;


    /**
     * 微信公众号配置
     */
    @RequestMapping("/get")
    @RequiresPermissions("sys:wechatmenu:all")
    public R get() {
        WxMenuEntity menu=wxMenService.get();


        return R.ok().put("data","");
    }


}
