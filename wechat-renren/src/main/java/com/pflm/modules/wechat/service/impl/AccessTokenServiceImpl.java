package com.pflm.modules.wechat.service.impl;
import com.pflm.modules.wechat.dao.AccessTokenDao;
import com.pflm.modules.wechat.entity.AccessTokenEntity;
import com.pflm.modules.wechat.service.AccessTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author qinxuewu
 * @version 1.00
 * @time 15/11/2018下午 6:20
 */
@Service
public class AccessTokenServiceImpl implements AccessTokenService {
    @Autowired
    AccessTokenDao accessTokenDao;

    @Override
    public AccessTokenEntity getToken(int type) {

        return accessTokenDao.getToken(type);
    }
}
