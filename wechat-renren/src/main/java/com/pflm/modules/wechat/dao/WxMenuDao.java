package com.pflm.modules.wechat.dao;
import com.pflm.modules.wechat.entity.WxMenuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface WxMenuDao {


    @Select("select * from w_menu")
    WxMenuEntity get();
}
